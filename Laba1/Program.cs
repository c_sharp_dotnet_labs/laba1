﻿using System;

namespace Laba1
{
    class Program
    {
        

    // This method shows main functionality of Student class
        static void Main(string[] args)
        {
            Student st;

            try
            {
                st = new Student("Иванов1", "18.123КИУКИ.5001");
            } catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            try
            {
                st = new Student("Иванов Иван Иванович",
                                "123 5001");
            } catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }

            // correct arguments 
            st = new Student("Иванов Иван Иванович",
                                "18.123КИУКИ.5001");

            Console.WriteLine("Trying to set grade to 0 (invalid)...");
            try
            {
                st.Grade = 0;
            } catch(ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }

            st.Grade = 1;

            Console.WriteLine("Starting working with Ivanov...");
            MarksForEnum marks = new MarksForEnum
            {
                SS = 66,
                CA = 78,
                CC = 84,
                Log = 90,
                Itech = 70,
                SMS = 60,
                LM = 70,
                SEO = 80,
                DMS = 90,
                Phys = 75
            };
            st.AddMarks(marks);
            Console.WriteLine(st);
            Console.WriteLine("Work with Ivanov was ended.");

            var studentWithoutMarks = new Student("Сидоров Сидор Сидорович", "999992");
            Console.WriteLine(studentWithoutMarks);
            
            marks = new MarksForEnum
            {
                SS = 69,
                CA = 88,
                CC = 74,
                Log = 60,
                Itech = 80,
                SMS = 90,
                LM = 80,
                SEO = 70,
                DMS = 60,
                Phys = 85
            };
            Student sidorov = studentWithoutMarks;
            sidorov.AddMarks(marks);

            Console.WriteLine(sidorov);

            Console.WriteLine("\nОценка Сидорова по СПО:\t" +
                sidorov.GetMarkBySubjName(SbjEnum.SS));

            sidorov.SetMarkForSubject(SbjEnum.SS, 90);
            Console.WriteLine("Эта оценка была изменена на 90");
            Console.Write("Теперь у Сидорова средний балл: ");
            Console.WriteLine(sidorov.GPA.ToString("0.######"));
            sidorov.SetMarkForSubject(0, 96);
            Console.WriteLine("Оценка по СПО была изменена на 96");
            Console.WriteLine(sidorov);

            Console.WriteLine("Сравнение оценок Иванова с оценками Сидорова.");
            Console.WriteLine("Из каждой оценки Иванова будет вычтена соответсвующая оценка Сидорова.");
            Console.WriteLine("Массив разниц:");
            var arr = st.ComapareTo(sidorov);
            foreach (sbyte i in arr)
            {
                Console.Write(i + "\t");
            }

            Console.WriteLine("\nTrying to set mark 101...");
            try
            {
                sidorov.SetMarkForSubject(SbjEnum.SS, 101);
            } catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
