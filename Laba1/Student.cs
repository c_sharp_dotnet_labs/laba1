﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace Laba1
{  
    public enum SbjEnum : byte
    {
            [Description("Системное программное обеспечение")]
            SS, // System Software
            [Description("Архитектура компьютеров")]
            CA, // Computer Architecture
            [Description("Компьютерная схемотехника")]
            CC, // Computer circuitry
            [Description("Логика")]
            Log, // Logic
            [Description("Интернет технологии")]
            Itech, // Internet technology
            [Description("Специализированные микроконтроллерные системы")]
            SMS, // Specialized microcontroller systems
            [Description("Логическое моделирование")]
            LM, // Logical modeling
            [Description("Технологии продвижения интернет ресурсов")]
            SEO, // search engine optimization
            [Description("Системы управления базами данных")]
            DMS, // Database management systems
            [Description("Физкультура")]
            Phys // PHYSICAL EDUCATION        
    }

    struct MarksForEnum
    {
        public byte SS, CA, CC, Log, Itech, SMS, LM, SEO, DMS, Phys;
    }
        
    class Student
    {
        // Number of subjescts students learn
        public const int N = 10;
        public const byte MaxMark = 100;

        private string _fullName; //can't contain numbers              
        private string _recordBookNumber; //cannot contain spaces
        private byte _grade; //from 1 to 6
        private byte[] _marks = new byte[10];
        private double _gpa; //Grade Point Average

        public double GPA { get => _gpa; }

        public Student(string fullName, string bookNumber)
        {
            string nameReason = "Full name is not valild.";
            string numberReason = "Record book number is not valid.";
            
            if (string.IsNullOrEmpty(fullName))
            {
                throw new ArgumentException(nameReason);
            }
            if (string.IsNullOrEmpty(bookNumber) )
            {
                throw new ArgumentException(numberReason);
            }

            foreach (char i in fullName)
            {
                if (Char.IsDigit(i))
                {
                    throw new ArgumentException(nameReason);
                }
            }

            foreach (char i in bookNumber)
            {
                if (i == ' ')
                {
                    throw new ArgumentException(numberReason);
                }
            }

            _fullName = fullName;
            _recordBookNumber = bookNumber;
        }

        public byte Grade
        {
            get => _grade;
            set
            {
                if (1 <= value && value <=6)
                {
                    _grade = value;
                }
                else
                {
                    throw new ArgumentException("Exception: Invalid grade number!");
                }
            }
        }

        // Mark should be in range [0 ... 100]
        private static void CheckMark(byte b)
        {
            if (MaxMark < b)
            {
                throw new ArgumentException("Exception: Mark is greater than 100!");
            }
        }

        //Добавляет 10 оценок по предметам из перечесления SbjEnum
        //Копирует значения полей структуры MarksForEnum
        public void AddMarks(MarksForEnum m)
        {   
            _marks[0] = m.SS;
            _marks[1] = m.CA;
            _marks[2] = m.CC;
            _marks[3] = m.Log;
            _marks[4] = m.Itech;
            _marks[5] = m.SMS;
            _marks[6] = m.LM;
            _marks[7] = m.SEO;
            _marks[8] = m.DMS;
            _marks[9] = m.Phys;

            int sum = 0;
            foreach (byte i in _marks)
            {
                CheckMark(i);
                sum += i;
            }
            _gpa = (double) sum / _marks.Length;
        }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.Append("\nФИО: ").Append(_fullName);
            sb.Append("\nНомер зачетной книжки: ").Append(_recordBookNumber);
            sb.Append("\nKурс: ");
            if (0 == _grade)
            {
                sb.Append("не указан");
            } else
            {
                sb.Append(_grade);
            }

            sb.Append("\nОценки:");
            SbjEnum[] subjects = new SbjEnum[] { 
                SbjEnum.SS, SbjEnum.CA, SbjEnum.CC, SbjEnum.Log, SbjEnum.Itech, 
                SbjEnum.SMS, SbjEnum.LM, SbjEnum.SEO, SbjEnum.DMS, SbjEnum.Phys
            };
            for (int i = 0; i < 10; ++i)
            {
                sb.Append("\n\t")
                  .Append(GetDescription(subjects[i]))
                  .Append(":\t");
                if (0 == _marks[i])
                {
                    sb.Append("оценка не поставлена");
                }
                else
                {
                    sb.Append(_marks[i]);
                }
            }
            sb.Append("\nСредний балл: ").Append(_gpa.ToString("0.######"));
            return sb.ToString();
        }

        private void RecalculateGPA (byte a, byte b)
        {
            // Можно так:

            //int sum = 0;
            //foreach (int i in _marks)
            //{
            //    sum += i;
            //}
            //_gpa = (double)sum / _marks.Length;

            // Но так будет быстрее:
            sbyte c = (sbyte) (b - (int) a);
            _gpa += c / 10.0;
        }

        public void SetMarkBySubjectIndex(byte i, byte mark)
        {
            CheckMark(mark);
            byte oldMark = 0;
            try
            {
                oldMark = _marks[i];
                _marks[i] = mark;
            } catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            RecalculateGPA(oldMark, mark);
        }
        public void SetMarkForSubject(SbjEnum sbj, byte mark)
        {
            CheckMark(mark); 
            byte oldMark = 0;
            int i = sbj.GetHashCode();
            try
            {
                oldMark = _marks[i];
                _marks[i] = mark;
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            RecalculateGPA(oldMark, mark);
        }

        public byte GetMarkBySubjName(SbjEnum name)
        {
            byte toOut = 0;
            try
            {
                toOut = _marks[name.GetHashCode()];
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return toOut;
        }

        public sbyte[] ComapareTo(Student b)
        {
            var toOut = new sbyte[10];
            for (int i = 0; i < N; ++i)
            {
                toOut[i] = (sbyte) (_marks[i] - b._marks[i]);
            }
            return toOut;
        }

        /// Приведение значения перечисления в удобочитаемый формат.
        /// Для корректной работы необходимо использовать атрибут [Description("Name")]
        /// для каждого элемента перечисления.    
        static string GetDescription(Enum enumElement)
        {
        Type type = enumElement.GetType();

        MemberInfo[] memInfo = type.GetMember(enumElement.ToString());
        if (memInfo != null && memInfo.Length > 0)
        {
            object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attrs != null && attrs.Length > 0)
                return ((DescriptionAttribute)attrs[0]).Description;
        }

        return enumElement.ToString();
        }
    }
}
